package info.bitcoinunlimited.votepeer.ringSignatureVote.room

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import info.bitcoinunlimited.votepeer.election.ElectionFaker
import info.bitcoinunlimited.votepeer.vote.Vote
import info.bitcoinunlimited.votepeer.vote.VoteOption
import info.bitcoinunlimited.votepeer.vote.VoteOption.Companion.NOT_VOTED_MAGIC_INDEX
import io.mockk.mockk
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
class RingSignatureVoteOptionDaoTest {
    private lateinit var database: RingSignatureVoteDatabase
    private lateinit var dao: RingSignatureVoteDao
    private val applicationContext = ApplicationProvider.getApplicationContext<Context>()
    private val ringSignatureElection = ElectionFaker.generateOngoingRingSignatureElection("mock_id")

    @BeforeEach
    fun init() {
        database = Room.inMemoryDatabaseBuilder(applicationContext, RingSignatureVoteDatabase::class.java).build()
        dao = database.ringSignatureVoteDao()
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun insertAndReadRingSignatureVoteTest() = runBlocking {
        val dao = database.ringSignatureVoteDao()
        val option = VoteOption("0")
        val vote = Vote(option, mockk(relaxed = true))
        val ringSignatureVoteRoom = RingSignatureVoteRoom(ringSignatureElection, vote)
        dao.insert(ringSignatureVoteRoom)
        val selectedOption = dao.get(ringSignatureElection.id)?.selectedOption ?: return@runBlocking
        assertEquals(option.selected, selectedOption)
    }

    private fun fakeRingSignatureVotesRoom(n: Int): List<RingSignatureVoteRoom> {
        val votes = mutableListOf<RingSignatureVoteRoom>()
        for (i in 0..n) {
            val newRingSignatureElection = ringSignatureElection.copy(
                electionRaw = ringSignatureElection.electionRaw.copy(id = ringSignatureElection.id + "$i")
            )
            val option = VoteOption("0")
            val vote = Vote(option, mockk(relaxed = true))
            votes.add(RingSignatureVoteRoom(newRingSignatureElection, vote))
        }
        return votes.toList()
    }

    @Test
    fun insertSeveralRingSignatureVotesAndReadOneTest() = runBlocking {
        val dao = database.ringSignatureVoteDao()
        val votes = fakeRingSignatureVotesRoom(100)
        val vote = votes[0]
        votes.forEach {
            dao.insert(it)
        }

        val voteNotVotedElection = ringSignatureElection.copy(
            electionRaw = ringSignatureElection.electionRaw.copy(id = ringSignatureElection.id + "NOT_VOTED_MAGIC_INDEX")
        )
        val optionNotVoted = VoteOption(NOT_VOTED_MAGIC_INDEX)
        val voteNotVoted = Vote(optionNotVoted, mockk(relaxed = true))
        val ringSignatureVoteNotVoted = RingSignatureVoteRoom(voteNotVotedElection, voteNotVoted)
        dao.insert(ringSignatureVoteNotVoted)
        val voteFromDb: RingSignatureVoteRoom? = dao.get(ringSignatureElection.id + "0")
        assertEquals(vote.electionId, voteFromDb?.electionId)
    }
}