package info.bitcoinunlimited.votepeer.votePeerActivity

import bitcoinunlimited.libbitcoincash.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.functions.FirebaseFunctions
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.IdentityRepository
import info.bitcoinunlimited.votepeer.auth.AuthRepository
import info.bitcoinunlimited.votepeer.auth.AuthState
import info.bitcoinunlimited.votepeer.auth.HttpsProviderFirebase
import info.bitcoinunlimited.votepeer.election.ElectionFaker
import info.bitcoinunlimited.votepeer.utils.Constants
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.spyk
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.onEach
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class VotePeerActivityViewModelTest {

    companion object {
        val chain = ChainSelector.BCHMAINNET
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(chain.v)
        }
    }

    private lateinit var viewModel: VotePeerActivityViewModel
    private lateinit var authRepository: AuthRepository
    private lateinit var electrumAPI: ElectrumAPI
    private lateinit var mockUri: String

    @BeforeEach
    fun setUp() {
        mockkStatic(FirebaseAuth::class)
        mockkStatic(FirebaseFunctions::class)
        every { FirebaseAuth.getInstance() } returns mockk(relaxed = true)
        every { FirebaseFunctions.getInstance(Constants.region) } returns mockk(relaxed = true)

        val privateKey = ElectionFaker.mockPrivateKey('A')
        val payDestination: PayDestination = Pay2PubKeyHashDestination(chain, UnsecuredSecret(privateKey))
        electrumAPI = spyk(ElectrumAPI.getInstance(chain))
        val identityRepository = IdentityRepository.getInstance(payDestination)
        authRepository = spyk(AuthRepository.getInstance(identityRepository, httpsProviderFirebase = HttpsProviderFirebase))
        viewModel = spyk(VotePeerActivityViewModel(authRepository, electrumAPI, chain, mockk(relaxed = true)))

        mockUri = "bchidentity://${Constants.region}-voter-6700b.cloudfunctions.net:443/" +
            "identify?op=login&chal=xEKbFavYalUPgM25FNMJ07an5IvPoeSm&cookie=1IzSfwrioGUTeQrhSRCMRrPSiDR2"
    }

    @Test
    fun observeAuth() {
        viewModel.observeAuth()
        coVerify { authRepository.authState.onEach { AuthState.AuthError(Exception("asd")) } }
        val state = viewModel.state.value
        assertNull(state)
    }

    @Test
    fun bindIntentsTest() {
        val view = mockk<VotePeerActivityView>(relaxed = true)
        val viewState = VotePeerActivityViewState.ConnectionStatus(false)
        viewModel.state.value = viewState

        viewModel.bindIntents(view)
        coVerify { view.initState() }
        coVerify { view.submitConnectionStatus() }
        coVerify { viewModel.state.filterNotNull() }
        assertEquals(viewState, viewModel.state.value)
    }

    @Test
    fun observeElectrumApiExceptions() {
        val exception = Exception("Electrum api error")
        electrumAPI.exception.value = exception
        viewModel.observeElectrumApiExceptions()

        coVerify { electrumAPI.exception }
    }

    @Test
    fun startQrScanner() {
        viewModel.startQrScanner()
        assert(viewModel.state.value is VotePeerActivityViewState.DefaultNone)
    }
}
