package info.bitcoinunlimited.votepeer.election.fund.screens

import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import com.google.accompanist.pager.*
import info.bitcoinunlimited.votepeer.R
import info.bitcoinunlimited.votepeer.election.fund.ContractState
import info.bitcoinunlimited.votepeer.election.fund.FundExternalScreen
import info.bitcoinunlimited.votepeer.election.fund.FundVotePeerScreen
import kotlinx.coroutines.launch

typealias ComposableFun = @Composable () -> Unit

sealed class TabItem(var icon: Int, var title: String, var screen: ComposableFun) {
    data class External(
        val electionTitle: String,
        val optionHuman: String,
        val contractState: ContractState,
        val contractAddress: String,
        val qrCode: Painter,
        val copyAddress: () -> Unit,
        val shareAddress: () -> Unit,
    ) : TabItem(R.drawable.ic_external, "External Wallet", {
        FundExternalScreen(
            electionTitle,
            optionHuman,
            contractState,
            contractAddress,
            qrCode,
            copyAddress,
            shareAddress
        )
    })
    data class Internal(
        val electionTitle: String,
        val optionHuman: String,
        val shareAddress: () -> Unit,
    ) : TabItem(R.drawable.ic_votepeer, "VotePeer", {
        FundVotePeerScreen(
            electionTitle,
            optionHuman
        )
    })
}

@ExperimentalPagerApi
@Composable
fun Tabs(tabs: List<TabItem>, pagerState: PagerState, onPageChange: (index: Int) -> Unit) {
    val scope = rememberCoroutineScope()
    // OR ScrollableTabRow()
    TabRow(
        // Our selected tab is our current page
        selectedTabIndex = pagerState.currentPage,
        // Override the indicator, using the provided pagerTabIndicatorOffset modifier
        backgroundColor = colorResource(id = R.color.colorPrimaryDark),
        contentColor = Color.White,
        indicator = { tabPositions ->
            TabRowDefaults.Indicator(
                Modifier.pagerTabIndicatorOffset(pagerState, tabPositions)
            )
            onPageChange(pagerState.currentPage)
        }
    ) {
        // Add tabs for all of our pages
        tabs.forEachIndexed { index, tab ->
            // OR Tab()
            LeadingIconTab(
                icon = { Icon(painter = painterResource(id = tab.icon), contentDescription = "") },
                text = { Text(tab.title) },
                selected = pagerState.currentPage == index,
                onClick = {
                    scope.launch {
                        pagerState.animateScrollToPage(index)
                    }
                },
            )
        }
    }
}

@ExperimentalPagerApi
@Composable
fun TabsContent(tabs: List<TabItem>, pagerState: PagerState) {
    HorizontalPager(state = pagerState, count = tabs.size) { page ->
        tabs[page].screen()
    }
}

@ExperimentalPagerApi
@Preview(showBackground = true)
@Composable
fun TabsPreview() {
    val bitmap = painterResource(id = R.drawable.ic_external_black)

    val tabs = listOf(
        TabItem.External(
            "electionTitle",
            "optionHuman",
            ContractState.Contract(0.0069, 0.1337, "Voting for option: The rent is too damn high!"),
            "qpv5y82t8z7n6w80fpm64afah7ntptxue59h5cdsn2",
            bitmap,
            { },
            { }
        ),
        TabItem.Internal(
            "electionTitle",
            "optionHuman",
            { }
        )
    )
    val pagerState = rememberPagerState()
    Tabs(
        tabs,
        pagerState,
    ) { _ ->
    }
}

@ExperimentalPagerApi
@Preview(showBackground = true)
@Composable
fun TabsContentPreview() {
    val bitmap = painterResource(id = R.drawable.ic_external_black)

    val tabs = listOf(
        TabItem.External(
            "electionTitle",
            "optionHuman",
            ContractState.Contract(0.01, 0.01, "Voting for option: The rent is too damn high!"),

            "asdasdsad",
            bitmap,
            { },
            { }
        ),
        TabItem.Internal(
            "electionTitle",
            "optionHuman",
            { }
        )
    )
    val pagerState = rememberPagerState()
    TabsContent(
        tabs,
        pagerState,
    )
}