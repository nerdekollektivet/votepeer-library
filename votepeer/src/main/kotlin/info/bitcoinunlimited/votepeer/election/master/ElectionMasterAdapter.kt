package info.bitcoinunlimited.votepeer.election.master

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View.OnClickListener
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import bitcoinunlimited.libbitcoincash.PayDestination
import bitcoinunlimited.libbitcoincash.ToHexStr
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import info.bitcoinunlimited.votepeer.databinding.ItemElectionBinding.inflate
import info.bitcoinunlimited.votepeer.election.Election
import info.bitcoinunlimited.votepeer.election.MultiOptionVoteElection
import info.bitcoinunlimited.votepeer.election.RingSignatureVoteElection
import info.bitcoinunlimited.votepeer.election.TwoOptionVoteElection
import kotlinx.coroutines.*

@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class ElectionMasterAdapter(
    private val payDestination: PayDestination,
    private val viewModel: ElectionMasterViewModel
) : ListAdapter<Election, ElectionMasterAdapter.ViewHolder>(
    DIFF_CALLBACK
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val election = getItem(position) ?: return
        viewModel.viewModelScope.launch(Dispatchers.IO + viewModel.handler) {
            val blockHeight = viewModel.fetchBlockHeight()
            initHolder(holder, election, blockHeight)
        }
    }

    private fun initHolder(
        holder: ViewHolder,
        election: Election,
        blockHeight: Long
    ) = viewModel.viewModelScope.launch(Dispatchers.Main) {
        holder.bind(createOnClickListener(election, holder, blockHeight), election, blockHeight)
    }

    private fun navigateToElection(election: Election, holder: ViewHolder, blockHeight: Long) {
        val privateKey = payDestination.secret?.getSecret() ?: throw Exception("Cannot get privatekey")
        val privateKeyHex = ToHexStr(privateKey)
        val navigateToElectionDetailAction =
            ElectionMasterFragmentDirections.actionElectionMasterFragmentToElectionDetailFragment(
                election.getTitle(),
                election,
                blockHeight,
                privateKeyHex
            )
        holder.itemView.findNavController().navigate(navigateToElectionDetailAction)
    }

    private fun createOnClickListener(
        election: Election,
        holder: ViewHolder,
        blockHeight: Long
    ) = OnClickListener { _ ->
        when (election) {
            is TwoOptionVoteElection -> {
                val builder = MaterialAlertDialogBuilder(holder.itemView.context)
                builder.setTitle("Two Option votes are deprecated")
                builder.setNeutralButton("CANCEL") { dialog, which -> dialog.dismiss() }
                builder.setPositiveButton("VIEW IN BROWSER") { dialog, which ->
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse("https://voter.cash/#/election/${election.id}")
                    startActivity(holder.itemView.context, intent, null)
                    dialog.dismiss()
                }

                builder.show()
            }
            is MultiOptionVoteElection -> navigateToElection(election, holder, blockHeight)
            is RingSignatureVoteElection -> navigateToElection(election, holder, blockHeight)
        }
    }

    class ViewHolder(private var binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(onClickListener: OnClickListener, election: Election, blockHeight: Long) {
            val isAnonymous = election is RingSignatureVoteElection
            binding.setVariable(BR.blockHeight, blockHeight)
            binding.setVariable(BR.clickListener, onClickListener)
            binding.setVariable(BR.election, election)
            binding.setVariable(BR.anonymousVisibility, isAnonymous)
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Election>() {
            override fun areItemsTheSame(oldElection: Election, newElection: Election): Boolean =
                oldElection.id == newElection.id

            override fun areContentsTheSame(oldElection: Election, newElection: Election): Boolean {
                return oldElection.areContentsTheSame(newElection)
            }
        }
    }
}
