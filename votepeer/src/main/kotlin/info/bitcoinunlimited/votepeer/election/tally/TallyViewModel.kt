package info.bitcoinunlimited.votepeer.election.tally

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import info.bitcoinunlimited.votepeer.IdentityRepository
import info.bitcoinunlimited.votepeer.MultiOptionVoteRepository
import info.bitcoinunlimited.votepeer.VoteRepository
import info.bitcoinunlimited.votepeer.election.Election
import info.bitcoinunlimited.votepeer.election.RingSignatureVoteElection
import info.bitcoinunlimited.votepeer.ringSignatureVote.RingSignatureVoteRepository
import info.bitcoinunlimited.votepeer.utils.TAG_ELECTION_RESULT
import info.bitcoinunlimited.votepeer.vote.VoteOption.Companion.NOT_VOTED_MAGIC_INDEX
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class TallyViewModel(
    private val identityRepository: IdentityRepository,
    private val repository: VoteRepository,
    private val election: Election,
) : ViewModel() {
    private val _loading = MutableStateFlow(true)
    private val _result = MutableStateFlow<Map<String, Int>>(mapOf())
    private val _error = MutableStateFlow<Exception?>(null)
    val loading = _loading.asStateFlow()
    val result = _result.asStateFlow()
    val error = _error.asStateFlow()

    private val handler = CoroutineExceptionHandler { context, throwable ->
        Log.e(TAG_ELECTION_RESULT, context.toString())
        Log.e(TAG_ELECTION_RESULT, throwable.message ?: "Something went wrong in ElectionResultViewModel")
        viewModelScope.launch(Dispatchers.Main) {
            _loading.value = false
            _error.value = Exception(throwable)
        }
    }

    init {
        checkElectionIfRingSignature()
    }

    private fun checkElectionIfRingSignature() {
        if (election is RingSignatureVoteElection && election.participants.isEmpty()) {
            viewModelScope.launch(Dispatchers.Default + handler) {
                throw IllegalStateException("No participants!")
            }
        }
    }

    internal fun setLoading(
        loading: Boolean
    ) = viewModelScope.launch(Dispatchers.Main + handler) {
        _loading.value = loading
    }

    internal fun refreshTally() = viewModelScope.launch(Dispatchers.IO + handler) {
        setLoading(true)
        val pkh = identityRepository.getPkh()
        val tally = when (repository) {
            is RingSignatureVoteRepository -> { repository.getTally(pkh) }
            is MultiOptionVoteRepository -> { repository.getTally(pkh) }
            else -> { throw Exception("Cannot get Repository.getTally()") }
        }
        val result = mutableMapOf<String, Int>()
        tally.option.forEach {
            when (it.key) {
                NOT_VOTED_MAGIC_INDEX -> {
                    result["Not voted"] = it.value.size
                }
                else -> {
                    result[it.key] = it.value.size
                }
            }
        }
        viewModelScope.launch(Dispatchers.Main) {
            _result.value = result
        }
        setLoading(false)
    }

    class TallyViewModelFactory(
        private val identityRepository: IdentityRepository,
        private val election: Election,
        private val voteRepository: VoteRepository,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(TallyViewModel::class.java))
                return TallyViewModel(
                    identityRepository,
                    voteRepository,
                    election,
                ) as T
            else
                throw IllegalArgumentException("ViewModel class must be a subclass of ElectionDetailViewModel!")
        }
    }
}
