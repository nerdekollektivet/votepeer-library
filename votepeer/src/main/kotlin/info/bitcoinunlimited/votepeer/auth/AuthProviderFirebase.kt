package info.bitcoinunlimited.votepeer.auth

import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.*
import kotlinx.coroutines.tasks.await

@DelicateCoroutinesApi
object AuthProviderFirebase {
    fun getCurrentUserId(): String? {
        return FirebaseAuth.getInstance().currentUser?.uid
    }

    fun isSignedInAnonymously(): Boolean {
        return FirebaseAuth.getInstance().currentUser?.isAnonymous ?: false
    }

    @Suppress("unused")
    fun currentUserStartsWithBitCoinCash(): Boolean {
        return FirebaseAuth.getInstance().currentUser?.uid?.startsWith("bitcoincash") == true
    }

    private fun startsWithBitcoinCash(currentUserId: String): Boolean {
        return currentUserId.startsWith("bitcoincash")
    }

    fun signOut() {
        FirebaseAuth.getInstance().signOut()
    }

    suspend fun signInAnonymously(): String {
        val response = FirebaseAuth.getInstance().signInAnonymously().await()
        val isAnonymous = response.user?.isAnonymous ?: false
        if (!isAnonymous) throw Exception("User is not anonymous")
        return response.user?.uid ?: throw Exception("Cannot get user?.uid after login")
    }

    fun getCookie(): String? {
        return FirebaseAuth.getInstance().currentUser?.uid
    }

    suspend fun signInWithJsonWebToken(jsonWebToken: String): String {
        val firebaseUser = FirebaseAuth.getInstance().signInWithCustomToken(jsonWebToken).await().user
        return firebaseUser?.uid ?: throw Exception("Cannot get firebaseUser?.ui")
    }

    fun fetchAuthState(auth: FirebaseAuth): AuthState {
        val currentUser = auth.currentUser ?: return AuthState.Unauthenticated
        val currentUserId = currentUser.uid
        val currentUserStartsWithBitcoinCash = startsWithBitcoinCash(currentUserId)

        if (currentUser.isAnonymous)
            return AuthState.AuthenticatedAnonymously(currentUserId)

        if (currentUserStartsWithBitcoinCash)
            return AuthState.AuthenticatedKeyPair(currentUserId)

        return AuthState.AuthError(Exception("Cannot identify AuthState in AuthProviderFirebase"))
    }
}