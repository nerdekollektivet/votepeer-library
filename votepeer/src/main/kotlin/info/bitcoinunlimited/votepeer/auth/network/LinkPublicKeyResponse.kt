package info.bitcoinunlimited.votepeer.auth.network

import kotlinx.serialization.Serializable

@Serializable
data class LinkPublicKeyResponse(
    val status: String
)