package info.bitcoinunlimited.votepeer.votePeerActivity

import android.app.Application
import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import bitcoinunlimited.libbitcoincash.ChainSelector
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.auth.AuthRepository
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class VotePeerActivityViewModelFactory(
    private val authRepository: AuthRepository,
    private val electrumApi: ElectrumAPI,
    owner: SavedStateRegistryOwner,
    private val chain: ChainSelector,
    private val application: Application,
    defaultArgs: Bundle? = null,
) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {

    override fun <T : ViewModel?> create(key: String, modelClass: Class<T>, state: SavedStateHandle) =
        VotePeerActivityViewModel(authRepository, electrumApi, chain, application) as T
}
