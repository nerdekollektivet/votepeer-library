package info.bitcoinunlimited.votepeer.tally

typealias Participant = String
typealias TxID = String
typealias Option = String

data class Tally(
    val electionId: String,
    val option: Map<Option, List<Pair<TxID?, Participant?>>>
)